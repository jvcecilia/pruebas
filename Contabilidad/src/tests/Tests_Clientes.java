package tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class Tests_Clientes {
	static Factura factura;
	static GestorContabilidad gestorContabilidad;
	static Cliente metodosCliente;

	@BeforeClass
	public static void prepararClasePruebas() {
		factura = new Factura();
		gestorContabilidad = new GestorContabilidad();
		metodosCliente = new Cliente();
	}
	@After
	public void despuesTests() {
		gestorContabilidad.getListaClientes().clear();
	}

	/*
	 * Calcula el precio total de una factura
	 */
	@Test
	public void precioTotalNormal() {
		factura.setCantidad(2);
		factura.setPrecioUnidad(2.6);

		double actual = factura.calcularPrecioTotal();
		double esperado = 5.2;

		assertEquals(esperado, actual, 0.01);
	}

	/*
	 * Comprueba la admision de numeros negativos cuando calculas el precio de una
	 * factura
	 */
	@Test
	public void precioTotalNegativo() {
		factura.setCantidad(-2.2);
		factura.setPrecioUnidad(2.5);

		double actual = factura.calcularPrecioTotal();
		double esperado = -5.5;

		assertEquals(esperado, actual, 0.01);

	}

	/*
	 * Comprueba que admita el numero 0 en el metodo de calcular el precio de la
	 * factura
	 */
	@Test
	public void precioTotalCantidadCero() {
		factura.setCantidad(0);
		factura.setPrecioUnidad(2.5);

		double actual = factura.calcularPrecioTotal();
		double esperado = 0;

		assertEquals(actual, esperado, 0);
	}

	/*
	 * El metodo de buscar clientes por DNI no encuentra a un cliente.
	 */
	@Test
	public void buscarClienteDniNoEncontrado() {

		String dni = "123123";

		Cliente actual = gestorContabilidad.buscarCliente(dni);

		assertNull(actual);

	}

	/*
	 * Devuelve null si buscas un cliente que no esta en el arraylist.
	 */
	@Test
	public void testBuscarClienteInexistenteConClientes() {

		Cliente esperado = new Cliente("calce", "1234F", "2000-05-02");

		gestorContabilidad.getListaClientes().add(esperado);

		Cliente actual = gestorContabilidad.buscarCliente("6");

		assertNull(actual);

	}

	/*
	 * Comprueba el funcionamiento correcto del metodo de buscar un cliente a partir
	 * del dni.
	 */
	@Test
	public void testBuscarCliente() {
		Cliente esperado = new Cliente("Maria", "12", "2005-04-04");
		gestorContabilidad.getListaClientes().add(esperado);
		String dniABuscar = "12";

		Cliente actual = gestorContabilidad.buscarCliente(dniABuscar);

		assertSame(esperado, actual);
	}

	/*
	 * Comprueba que se ha metido al cliente en el arraylist.
	 */
	@Test
	public void comprobarAltaClientesCorrecto() {
		Cliente esperado = new Cliente("Pepito", "2535", "2005-04-03");
		gestorContabilidad.altaCliente(esperado);
		gestorContabilidad.getListaClientes().add(esperado);
		assertTrue(gestorContabilidad.getListaClientes().contains(esperado));
	}

	/*
	 * Hay un cliente en el arraylist que tiene el mismo dni que el cliente a
	 * introducir en el metodo
	 */

	@Test
	public void altaClientesDniRepetido() {
		Cliente error = new Cliente("Pepito", "2535", "2005-04-03");
		Cliente esperado = new Cliente("Juanjo", "2535", "2003-04-03");
		gestorContabilidad.getListaClientes().add(error);

		gestorContabilidad.altaCliente(esperado);
		assertFalse(gestorContabilidad.getListaClientes().contains(esperado));

	}

	/*
	 * Se comprueba que el metodo devuelva null si el cliente no se encuentra en el
	 * vector
	 */
	@Test
	public void clienteMasAntiguoNull() {
		gestorContabilidad.getListaClientes().clear();
		Cliente cliente = gestorContabilidad.clienteMasAntiguo();
		assertNull(cliente);
	}

	/*
	 * Se comprueba el funcionamiento correcto del metodo para buscar el cliente mas
	 * antiguo
	 */
	@Test
	public void devolverClienteMasAntiguo() {
		Cliente actual = null;
		Cliente clientePrueba3 = new Cliente("Pepe", "2", "1995-03-03");
		Cliente clientePrueba = new Cliente("Juan", "3", "1996-03-03");
		Cliente clientePrueba2 = new Cliente("Juan", "3", "1995-12-12");
		Cliente esperado = new Cliente("Juan", "3", "1994-03-03");

		gestorContabilidad.getListaClientes().add(clientePrueba);
		gestorContabilidad.getListaClientes().add(clientePrueba2);
		gestorContabilidad.getListaClientes().add(clientePrueba3);
		gestorContabilidad.getListaClientes().add(esperado);

		actual = gestorContabilidad.clienteMasAntiguo();
		assertSame(actual, esperado);
	}

	/*
	 * Elimina un cliente de la lista de clientes exitosamente
	 */
	@Test
	public void eliminarCliente() {
		Cliente clientePrueba = new Cliente("Juan", "3", "1996-03-03");
		gestorContabilidad.getListaClientes().add(clientePrueba);
		gestorContabilidad.eliminarCliente(clientePrueba.getDni());
		boolean encontrado = gestorContabilidad.getListaClientes().contains(clientePrueba);
		assertFalse(encontrado);
	}

	/*
	 * Tratas de eliminar un cliente que no existe en la lista
	 */
	@Test
	public void eliminarClienteInexistente() {
		Cliente clientePrueba = new Cliente("Juan", "3", "1996-03-03");
		gestorContabilidad.getListaClientes().add(clientePrueba);
		gestorContabilidad.eliminarCliente("22");
		assertTrue(gestorContabilidad.getListaClientes().size() == 1);

	}

}
