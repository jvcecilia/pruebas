package tests;

import clases.*;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Tests_Facturas {

	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void prepararTests() {
		gestorContabilidad = new GestorContabilidad();

	}
	
	@After
	public void despuesDeFinalizarTests() {
		gestorContabilidad.getListaFacturas().clear();
	}

	/*
	 * Encuentra la factura de forma exitosa dentro del vector.
	 */
	@Test
	public void buscarFacturaCorrecto() {
		Factura esperado = new Factura("8", "1995-02-03", "Anchoas", 5.4, 3, new Cliente());
		gestorContabilidad.getListaFacturas().add(esperado);
		Factura actual = gestorContabilidad.buscarFactura("8");
		assertSame(esperado, actual);

	}

	/*
	 * La factura del codigo buscado no se encuentra dentro del arraylist.
	 */
	@Test
	public void BuscarFacturaNull() {
		Factura esperado = new Factura("2", "1995-02-03", "Anchoas", 5.4, 3, new Cliente());
		gestorContabilidad.getListaFacturas().add(esperado);
		Factura actual = gestorContabilidad.buscarFactura("25");
		assertNull(actual);
	}

	/*
	 * Comprueba que se crea una factura exitosamente.
	 */

	@Test
	public void crearFacturaCorrectamente() {
		Factura esperado = new Factura("4", "1995-02-03", "Anchoas", 5.4, 3, new Cliente());

		gestorContabilidad.crearFactura(esperado);
		boolean encontrado = gestorContabilidad.getListaFacturas().contains(esperado);

		assertTrue(encontrado);
	}

	/*
	 * Comprueba que al intentar crear una factura no se consiga debido a que el
	 * codigo de la factura ya existe en otra factura del arraylist
	 */
	@Test
	public void crearFacturaExistente() {
		Factura esperado = new Factura("2", "1995-02-03", "Pepitas", 5.4, 3, new Cliente());
		gestorContabilidad.crearFactura(esperado);
		boolean encontrado = gestorContabilidad.getListaFacturas().contains(esperado);
		assertTrue(encontrado);
	}

	/*
	 * Comprueba que encuentra la factura mas cara en el vector de facturas
	 */
	@Test
	public void facturaCaraEncontrada() {
		Factura factura = new Factura("1", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		gestorContabilidad.getListaFacturas().add(factura);
		Factura factura2 = new Factura("2", "1995-03-03", "Carne", 5, 4, new Cliente());
		gestorContabilidad.getListaFacturas().add(factura2);
		Factura esperado = new Factura("3", "1995-03-03", "Carne", 6, 4, new Cliente());
		gestorContabilidad.getListaFacturas().add(esperado);
		Factura actual = gestorContabilidad.facturaMasCara();
		assertEquals(esperado, actual);
	}

	/*
	 * Factura mas cara devuelve null
	 */
	@Test
	public void facturaMasCaraNull() {
		gestorContabilidad.getListaFacturas().clear();
		Factura actual = gestorContabilidad.facturaMasCara();
		assertNull(actual);
	}
	
	/*
	 * Devuelve 0 al no existir ninguna factura en el año seleccionado
	 */
	@Test
	public void facturacionAnualAñoInexistenteEnVector() {
		Factura factura = new Factura("1", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		Factura factura2 = new Factura("2", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		gestorContabilidad.getListaFacturas().add(factura);
		gestorContabilidad.getListaFacturas().add(factura2);
		float actual = gestorContabilidad.calcularFacturacionAnual(1996);
		assertEquals(actual, 0, 0);
	}
	
	/*
	 * Comprueba que te devuelve correctamente la facturacion anual del año introducido por parametro
	 */
	@Test
	public void facturacionAnualAñoExistente() {
		float esperado = 20f;
		Factura factura = new Factura("1", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		Factura factura2 = new Factura("2", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		gestorContabilidad.getListaFacturas().add(factura);
		gestorContabilidad.getListaFacturas().add(factura2);
		float actual = gestorContabilidad.calcularFacturacionAnual(1995);
		assertEquals(esperado, actual,0);
	}
	
	/*
	 * Se comprueba que una factura se elimina exitosamente.
	 */
	@Test
	public void eliminarFacturaCorrectamente() {
		Factura factura = new Factura("1", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		gestorContabilidad.getListaFacturas().add(factura);
		gestorContabilidad.eliminarFactura("1");
		boolean noEncontrado = gestorContabilidad.getListaFacturas().contains(factura);
		assertFalse(noEncontrado);
	}
	
	/*
	 * Trata de eliminar una factura que no existe en el vector
	 */
	@Test
	public void eliminarFacturaInexistente() {
		Factura factura = new Factura("1", "1995-03-03", "Carne", 2.5, 4,new Cliente());
		Factura factura2 = new Factura("2", "1995-03-03", "Carne", 2.5, 4,new Cliente());
		gestorContabilidad.getListaFacturas().add(factura2);
		gestorContabilidad.eliminarFactura(factura.getCodigoFactura());
		assertTrue(gestorContabilidad.getListaFacturas().size() == 1);
	}
	

}
