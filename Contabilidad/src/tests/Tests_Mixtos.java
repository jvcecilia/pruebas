package tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import clases.*;

public class Tests_Mixtos {
	static GestorContabilidad gestorContabilidad;

	@BeforeClass
	public static void antesDeTests() {
		gestorContabilidad = new GestorContabilidad();

	}

	@After
	public void finalDeTests() {
		gestorContabilidad.getListaClientes().clear();
		gestorContabilidad.getListaFacturas().clear();
	}

	/*
	 * Asigna correctamente un cliente a una factura.
	 */

	@Test
	public void comprobarClienteAsignadoAFacturaCorrecto() {
		Cliente cliente = new Cliente("Pepe", "2525", "1995-04-04");
		Factura factura2 = new Factura("2", "1995-03-03", "Carne", 2.5, 4, new Cliente());
		gestorContabilidad.getListaClientes().add(cliente);
		gestorContabilidad.getListaFacturas().add(factura2);

		gestorContabilidad.asignarClienteAFactura("2525", "2");

		String esperado = "2525";
		String actual = gestorContabilidad.getListaFacturas().get(0).getCliente().getDni();
		assertEquals(esperado, actual);

	}

	/*
	 * Intento de asignar un cliente a una factura que no existe.
	 */
	@Test
	public void asignarClienteAFacturaSinFactura() {
		Cliente unCliente = new Cliente("1", "As", "2000-01-01");
		gestorContabilidad.getListaClientes().add(unCliente);
		gestorContabilidad.asignarClienteAFactura("1", "2");
		assertTrue(gestorContabilidad.getListaFacturas().size() == 0);
	}
	
	/*
	 * El metodo devuelve correctamente el resultado esperado
	 */
	@Test
	public void variosClientesCantidadFacturasPorCliente() {
		Cliente unCliente = new Cliente("1", "As", "2000-01-01");
		Cliente otroCliente = new Cliente("3", "As", "2000-01-02");
		Factura factura = new Factura("2", "1995-03-03", "Carne", 2.5, 4, unCliente);
		Factura factura2 = new Factura("2", "1995-03-03", "Carne", 2.5, 4, otroCliente);
		gestorContabilidad.getListaFacturas().add(factura2);
		gestorContabilidad.getListaFacturas().add(factura);
		int esperado = 2;
		int actual = gestorContabilidad.cantidadFacturasPorCliente("As");
		assertEquals(esperado, actual);
	}
	
	/*
	 * Busca sacar la cantidad de facturas de un cliente que no existe.
	 */
	@Test
	public void cantidadFacturasPorClienteInexistente() {
		gestorContabilidad.getListaFacturas().add(new Factura("2", "1995-03-03", "Carne", 2.5, 4,new Cliente("1", "As", "2000-01-01")));
		int esperado = 0;
		int actual = gestorContabilidad.cantidadFacturasPorCliente("15");
		assertEquals(esperado, actual);
	}
	

}
