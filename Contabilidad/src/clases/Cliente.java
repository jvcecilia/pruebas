package clases;


public class Cliente {

	private String nombre;
	private String dni;
	private String fecha;
		
	public Cliente(String nombre, String dni, String fecha) {
		
		this.nombre = nombre;
		this.dni = dni;
		this.fecha = fecha;
		
	}
	
	
	public Cliente(){
		
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	
	
	
}
