package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorContabilidad {
	private ArrayList<Factura> listaFacturas;
	private ArrayList<Cliente> listaClientes;

	public GestorContabilidad() {
		this.listaFacturas = new ArrayList<Factura>();
		this.listaClientes = new ArrayList<Cliente>();
	}

	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}

	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}

	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}

	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}

	public Cliente buscarCliente(String dni) {

		for (int i = 0; i < listaClientes.size(); i++) {
			if (listaClientes.get(i).getDni().equals(dni)) {
				return listaClientes.get(i);
			}
		}
		return null;
	}

	public Factura buscarFactura(String codigo) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if (listaFacturas.get(i).getCodigoFactura().equals(codigo)) {
				return listaFacturas.get(i);
			}
		}
		return null;
	}

	public void altaCliente(Cliente cliente) {
		for (int i = 0; i < getListaClientes().size(); i++) {
			if (cliente.getDni().equals(getListaClientes().get(i).getDni())) {

			}
		}
	}

	public void crearFactura(Factura factura) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if (listaFacturas.get(i).getCodigoFactura().equals(factura.getCodigoFactura())) {
				return;
			}
		}
		listaFacturas.add(factura);
	}

	public Cliente clienteMasAntiguo() {
		Cliente cliente = null;
		for (int i = 0; i < listaClientes.size(); i++) {
			if (i == 0) {
				cliente = listaClientes.get(i);
			} else {
				if ((LocalDate.parse(listaClientes.get(i).getFecha()))
						.compareTo((LocalDate.parse(cliente.getFecha()))) < 0) {
					cliente = listaClientes.get(i);
				}
			}

		}
		return cliente;
	}

	public Factura facturaMasCara() {
		Factura factura = null;
		double maximo = 0;
		for (int i = 0; i < listaFacturas.size(); i++) {
			if (i == 0) {
				maximo = listaFacturas.get(i).calcularPrecioTotal();
				factura = listaFacturas.get(i);
			} else {
				if (listaFacturas.get(i).calcularPrecioTotal() > maximo) {
					maximo = listaFacturas.get(i).calcularPrecioTotal();
					factura = listaFacturas.get(i);
				}
			}
		}
		return factura;
	}

	public float calcularFacturacionAnual(int anyo) {
		int facturacionAnual = 0;
		for (Factura factura : this.listaFacturas) {
			if (LocalDate.parse(factura.getFecha()).getYear() == anyo) {
				facturacionAnual += factura.calcularPrecioTotal();
			}
		}
		return facturacionAnual;
	}

	public void asignarClienteAFactura(String dni, String codigoFactura) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if (listaFacturas.get(i).getCodigoFactura().equals(codigoFactura)) {
				for (int j = 0; j < listaClientes.size(); j++) {
					if (listaClientes.get(i).getDni().equals(dni)) {
						listaFacturas.get(i).setCliente(listaClientes.get(i));
					}
				}
			}
		}
		
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int contador = 0;
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getCliente().getDni().equals(dni)) {
				contador++;
			}
		}
		return contador;
	}

	public void eliminarFactura(String codigo) {
		for (int i = 0; i < listaFacturas.size(); i++) {
			if(listaFacturas.get(i).getCodigoFactura().equals(codigo)) {
				listaFacturas.remove(listaFacturas.get(i));
			}
		}
	}
	
	public void eliminarCliente(String dni) {
		for (int i = 0; i < listaClientes.size(); i++) {
			if(listaClientes.get(i).getDni().equals(dni)) {
				if(cantidadFacturasPorCliente(dni)==0) {
					listaClientes.remove(listaClientes.get(i));
				}
			}
		}
	}
	
	
}
