package clases;

public class Factura {
	private String codigoFactura;
	private String fecha;
	private String nombreProducto;
	private double precioUnidad;
	private double cantidad;
	private Cliente cliente;
	
	public Factura(String codigoFactura, String fecha, String nombreProducto, double precioUnidad, double cantidad,
			Cliente cliente) {
		
		this.codigoFactura = codigoFactura;
		this.fecha = fecha;
		this.nombreProducto = nombreProducto;
		this.precioUnidad = precioUnidad;
		this.cantidad = cantidad;
		this.cliente = cliente;
	}
	
	public Factura(){
		
	}	

	public String getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(String codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public double getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(double precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public double getCantidad() {
		return cantidad;
	}

	public void setCantidad(double cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	
	public double calcularPrecioTotal(){
	
		return cantidad * precioUnidad;
	}
	
	public Factura buscarFactura(String codigo){
		return (new Factura());
	}

}